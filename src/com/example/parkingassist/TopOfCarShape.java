package com.example.parkingassist;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.shapes.Shape;

/***
 * This class creates a new shape which can be drawn
 * onto the UI of the app.
 * This shape is designed for the TopOfCarView page on the UI.
 * 
 * @author colinmcnicol
 *
 */
public class TopOfCarShape extends Shape{

	static int leftSensor;
	static int middleSensor;
	static int rightSensor;
	
	/***
	 * Constructor for creating the shape.
	 * The 3 parameters define the shape. They define the colour of each
	 * of the 9 segments based on the distance of an object from each sensor.
	 * 
	 * @param left - current value of the left sensor
	 * @param middle - current value of the middle sensor
	 * @param right - current value of the right sensor
	 */
	public TopOfCarShape(int left, int middle, int right){
		leftSensor = left;
		middleSensor = middle;
		rightSensor = right;
	}
	
	/***
	 * Overrides the Shape class draw method.
	 * This defines the unique aspects of this shape.
	 * The shape consists of 9 segments, 3 for each sensor.
	 * The colour of each segment is altered depending on the data from the sensors
	 */
	@Override
	public void draw(Canvas c, Paint p) {	    	    
	    if(leftSensor == 0){
	    	p.setColor(Color.GREEN);
    		drawTopLeftSegment(c, p);
	    	p.setColor(Color.TRANSPARENT);
	    	drawMiddleLeftSegment(c, p);
	    	drawBottomLeftSegment(c, p);
	    }
	    else if(leftSensor == 1){
	    	p.setColor(Color.GREEN);
    		drawTopLeftSegment(c, p);
	    	p.setColor(Color.YELLOW);
	    	drawMiddleLeftSegment(c, p);
	    	p.setColor(Color.TRANSPARENT);
	    	drawBottomLeftSegment(c, p);
	    }
	    else if(leftSensor == 2){
	    	p.setColor(Color.GREEN);
    		drawTopLeftSegment(c, p);
	    	p.setColor(Color.YELLOW);
	    	drawMiddleLeftSegment(c, p);
	    	p.setColor(Color.RED);
	    	drawBottomLeftSegment(c, p);
	    }
	    else if(leftSensor == 3){
	    	p.setColor(Color.TRANSPARENT);
    		drawTopLeftSegment(c, p);
	    	drawMiddleLeftSegment(c, p);
	    	drawBottomLeftSegment(c, p);
	    }
	    
	    if(middleSensor == 0){
	    	p.setColor(Color.GREEN);
    		drawTopMiddleSegment(c, p);
	    	p.setColor(Color.TRANSPARENT);
	    	drawMiddleMiddleSegment(c, p);
	    	drawBottomMiddleSegment(c, p);
	    }
	    else if(middleSensor == 1){
	    	p.setColor(Color.GREEN);
    		drawTopMiddleSegment(c, p);
	    	p.setColor(Color.YELLOW);
	    	drawMiddleMiddleSegment(c, p);
	    	p.setColor(Color.TRANSPARENT);
	    	drawBottomMiddleSegment(c, p);
	    }
	    else if(middleSensor == 2){
	    	p.setColor(Color.GREEN);
    		drawTopMiddleSegment(c, p);
	    	p.setColor(Color.YELLOW);
	    	drawMiddleMiddleSegment(c, p);
	    	p.setColor(Color.RED);
	    	drawBottomMiddleSegment(c, p);
	    }
	    else if(middleSensor == 3){
	    	p.setColor(Color.TRANSPARENT);
    		drawTopMiddleSegment(c, p);
	    	drawMiddleMiddleSegment(c, p);
	    	drawBottomMiddleSegment(c, p);
	    }
	    
	    if(rightSensor == 0){
	    	p.setColor(Color.GREEN);
    		drawTopRightSegment(c, p);
	    	p.setColor(Color.TRANSPARENT);
	    	drawMiddleRightSegment(c, p);
	    	drawBottomRightSegment(c, p);
	    }
	    else if(rightSensor == 1){
	    	p.setColor(Color.GREEN);
    		drawTopRightSegment(c, p);
	    	p.setColor(Color.YELLOW);
	    	drawMiddleRightSegment(c, p);
	    	p.setColor(Color.TRANSPARENT);
	    	drawBottomRightSegment(c, p);
	    }
	    else if(rightSensor == 2){
	    	p.setColor(Color.GREEN);
    		drawTopRightSegment(c, p);
	    	p.setColor(Color.YELLOW);
	    	drawMiddleRightSegment(c, p);
	    	p.setColor(Color.RED);
	    	drawBottomRightSegment(c, p);
	    }
	    else if(rightSensor == 3){
	    	p.setColor(Color.TRANSPARENT);
    		drawTopRightSegment(c, p);
	    	drawMiddleRightSegment(c, p);
	    	drawBottomRightSegment(c, p);
	    }
	    
	    
	    p.setColor(Color.BLACK);
	    p.setStyle(Paint.Style.STROKE);
	    
	    RectF rectF1 = new RectF(5, 5, 50, 570);
	    c.drawArc (rectF1, 90, 180, false, p);
	    
	    RectF rectF2 = new RectF(170, 60, 215, 515);
	    c.drawArc (rectF2, 90, 180, false, p);
	    
	    RectF rectF3 = new RectF(280, 95, 325, 480);
	    c.drawArc (rectF3, 90, 180, false, p);
	    
	    RectF rectF4 = new RectF(350, 120, 395, 455);
	    c.drawArc (rectF4, 90, 180, false, p);
	    
	    
	    c.drawLine(25, 5, 375, 120, p);
	    
	    c.drawLine(7, 210, 350, 230, p);
	    
	    c.drawLine(7, 380, 350, 350, p);
	    
	    c.drawLine(25, 570, 375, 455, p);
	}

	/***
	 * This method defines the bottom left segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawBottomLeftSegment(Canvas c, Paint p){
		Point[] bottomLeft = new Point[13];
		
		// left edge		
		bottomLeft[0] = new Point(300,480);
		bottomLeft[1] = new Point(297,475);
		bottomLeft[2] = new Point(295,470);
		bottomLeft[3] = new Point(290,450);
		bottomLeft[4] = new Point(287,430);
		bottomLeft[5] = new Point(282,370);
		bottomLeft[6] = new Point(282,356);
		// top edge
		bottomLeft[7] = new Point(352,350);
		// right edge
		bottomLeft[8] = new Point(353,380);
		bottomLeft[9] = new Point(357,410);
		bottomLeft[10] = new Point(362,440);
		bottomLeft[11] = new Point(366,450);
		bottomLeft[12] = new Point(371,456);
		
		// pass in array and draw onto the canvas
		drawShape(c, p, bottomLeft);
		
	}
	
	/***
	 * This method defines the bottom middle segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawBottomMiddleSegment(Canvas c, Paint p){
		Point[] bottomMiddle = new Point[9];
		
		// left edge		
		bottomMiddle[0] = new Point(282,356);
		bottomMiddle[1] = new Point(281,336);
		bottomMiddle[2] = new Point(280,306);
		bottomMiddle[3] = new Point(280,286);
		bottomMiddle[4] = new Point(281,256);
		bottomMiddle[5] = new Point(282,226);
		
		// top edge
		bottomMiddle[6] = new Point(351,230);
		
		// right edge
		bottomMiddle[7] = new Point(350,300);
		bottomMiddle[8] = new Point(352,350);
		
		// pass in array and draw onto the canvas
		drawShape(c, p, bottomMiddle);	
		
	}
	
	/***
	 * This method defines the bottom right segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawBottomRightSegment(Canvas c, Paint p){
		
		Point[] bottomRight = new Point[13];
		
		// left edge		
		bottomRight[0] = new Point(281,226);
		bottomRight[1] = new Point(282,220);
		bottomRight[2] = new Point(284,180);
		bottomRight[3] = new Point(288,140);
		bottomRight[4] = new Point(292,120);
		bottomRight[5] = new Point(298,100);
		bottomRight[6] = new Point(300,96);
		// top edge
		bottomRight[7] = new Point(372,119);
		
		// right edge
		bottomRight[8] = new Point(365,130);
		bottomRight[9] = new Point(360,150);
		bottomRight[10] = new Point(353,200);
		bottomRight[11] = new Point(352,220);
		bottomRight[12] = new Point(351,230);
		
		// pass in array and draw onto the canvas
		drawShape(c, p, bottomRight);
		
	}
	
	/***
	 * This method defines the middle left segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawMiddleLeftSegment(Canvas c, Paint p){
		Point[] middleLeft = new Point[17];
		
		// left edge
		middleLeft[0] = new Point(192,516);
		middleLeft[1] = new Point(190,515);
		middleLeft[2] = new Point(188,510);
		middleLeft[3] = new Point(186,505);
		middleLeft[4] = new Point(182,490);
		middleLeft[5] = new Point(181,480);
		middleLeft[6] = new Point(178,460);
		middleLeft[7] = new Point(177,440);
		middleLeft[8] = new Point(173,400);
		middleLeft[9] = new Point(172,366);
		
		// top edge
		middleLeft[10] = new Point(282,356);
		
		// right edge
		middleLeft[11] = new Point(282,370);
		middleLeft[12] = new Point(287,430);
		middleLeft[13] = new Point(290,450);
		middleLeft[14] = new Point(295,470);
		middleLeft[15] = new Point(297,475);
		middleLeft[16] = new Point(300,480);
		
		// pass in array and draw onto the canvas
		drawShape(c, p, middleLeft);
		
	}
	
	/***
	 * This method defines the middle middle segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawMiddleMiddleSegment(Canvas c, Paint p){
		Point[] middleMiddle = new Point[9];
		
		// left edge
		middleMiddle[0] = new Point(172,366);
		middleMiddle[1] = new Point(170,300);
		middleMiddle[2] = new Point(171,219);
		
		// top edge
		middleMiddle[3] = new Point(282,226);
		
		//right edge
		middleMiddle[4] = new Point(281,256);
		middleMiddle[5] = new Point(280,286);
		middleMiddle[6] = new Point(280,306);
		middleMiddle[7] = new Point(281,336);
		middleMiddle[8] = new Point(282,356);
		
		// pass in array and draw onto the canvas
		drawShape(c, p, middleMiddle);
		
	}
	
	/***
	 * This method defines the middle right segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawMiddleRightSegment(Canvas c, Paint p){
		Point[] middleRight = new Point[15];
		
		// left edge
		middleRight[0] = new Point(171,219);
		middleRight[1] = new Point(175,150);
		middleRight[2] = new Point(180,100);
		middleRight[3] = new Point(186,70);
		middleRight[4] = new Point(189,65);
		middleRight[5] = new Point(190,63);
		middleRight[6] = new Point(191,62);
		middleRight[7] = new Point(192,60);
		
		// top edge
		middleRight[8] = new Point(300,96);
		
		// right edge
		middleRight[9] = new Point(298,100);
		middleRight[10] = new Point(292,120);
		middleRight[11] = new Point(288,140);
		middleRight[12] = new Point(284,180);
		middleRight[13] = new Point(282,220);
		middleRight[14] = new Point(281,226);
		
		// pass in array and draw onto the canvas
		drawShape(c, p, middleRight);

	}
	
	/***
	 * This method defines the top right segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawTopRightSegment(Canvas c, Paint p){
		Point[] topRight = new Point[16];
		
		// left edge
		topRight[0] = new Point(7,211);
		topRight[1] = new Point(7,200);
		topRight[2] = new Point(9,150);
		topRight[3] = new Point(12,100);
		topRight[4] = new Point(15,60);
		topRight[5] = new Point(20,20);
		topRight[6] = new Point(23,10);
		topRight[7] = new Point(26,5);
		// top edge
		topRight[8] = new Point(192,60);
		
		// right edge
		topRight[9] = new Point(190,62);
		topRight[10] = new Point(188,65);
		topRight[11] = new Point(185,70);
		topRight[12] = new Point(183,80);
		topRight[13] = new Point(180,95);
		topRight[14] = new Point(172,180);
		topRight[15] = new Point(171,219);
	
		// pass in array and draw onto the canvas
		drawShape(c, p, topRight);

	}
	
	/***
	 * This method defines the top middle segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawTopMiddleSegment(Canvas c, Paint p){
		Point[] topMiddle = new Point[15];
		// left edge
		topMiddle[0] = new Point(7,380);
		topMiddle[1] = new Point(7,350);
		topMiddle[2] = new Point(6,355);
		topMiddle[3] = new Point(6,340);
		topMiddle[4] = new Point(6,300);
		topMiddle[5] = new Point(6,250);
		topMiddle[6] = new Point(6,240);
		topMiddle[7] = new Point(7,211);
		
		// top edge 
		topMiddle[8] = new Point(172,220);
		
		// right edge
		topMiddle[9] = new Point(170,240);
		topMiddle[10] = new Point(170,260);
		topMiddle[11] = new Point(170,280);
		topMiddle[12] = new Point(170,300);
		topMiddle[13] = new Point(171,330);
		topMiddle[14] = new Point(172,366);

		// pass in array and draw onto the canvas
		drawShape(c, p, topMiddle);
    	
	}
	
	/***
	 * This method defines the top left segment and calls the method to
	 * draw this segment.
	 * 
	 * @param c - Canvas in which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 */
	public void drawTopLeftSegment(Canvas c, Paint p){
	    Point[] topLeft = new Point[17];
	    // left edge
	    topLeft[0] = new Point(26,570);
	    topLeft[1] = new Point(24,568);
	    topLeft[2] = new Point(23,567);
	    topLeft[3] = new Point(19,550);
	    topLeft[4] = new Point(13,500);
	    topLeft[5] = new Point(10,450);
	    topLeft[6] = new Point(7,380);
	    // top edge
	    topLeft[7] = new Point(172,366);
	    // right edge
	    topLeft[8] = new Point(177,450);
	    topLeft[9] = new Point(179,470);
	    topLeft[10] = new Point(182,490);	    
	    topLeft[11] = new Point(183,495);
	    topLeft[12] = new Point(184,500);
	    topLeft[13] = new Point(186,505);
	    topLeft[14] = new Point(188,510);
	    topLeft[15] = new Point(190,515);
	    topLeft[16] = new Point(192,516);

	    // pass in array and draw onto the canvas
	    drawShape(c, p, topLeft);

	}
	
	/***
	 * This method takes the array provided and draws the points onto the canvas
	 * creating a shape.
	 * 
	 * @param c - Canvas which the shape is drawn onto
	 * @param p - Paint - can alter the style of painting
	 * @param points - array of points that make up the shape
	 */
	public void drawShape(Canvas c, Paint p, Point[] points){
        p.setStyle(Paint.Style.FILL);
        
        // create a path between all the points in the array
	    Path path = new Path();
	    // move through each point
	    path.moveTo(points[0].x,points[0].y);
	    for(int i = 0; i < points.length - 1; i++){
	    	path.lineTo(points[i+1].x,points[i+1].y);
	    }
	    
	    path.moveTo(points[points.length-1].x,points[points.length-1].y);
    	path.lineTo(points[0].x,points[0].y);
    	path.close();

    	// draw the path onto the canvas
    	c.drawPath(path, p);
	}

}
