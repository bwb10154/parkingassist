package com.example.parkingassist;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.view.View;

/***
 * This class creates and instance of the BackOfCarShape 
 * and draws it onto a canvas.
 * 
 * @author colinmcnicol
 *
 */
public class BackOfCarCustomShape extends View{

    private ShapeDrawable mDrawable;
    private int left = 0;
    private int middle = 0;
    private int right = 0;

    /***
     * Constructor for creating the BackOfCarCustomShape.
     */
    public BackOfCarCustomShape(Context context) {
	    super(context);
	    defineShape(left,middle,right);
    }
    
    /***
     * Constructor for creating the BackOfCarCustomShape.
     */
    public BackOfCarCustomShape(Context context, AttributeSet attrs) {
        super(context, attrs,0);
        defineShape(left,middle,right);
    }

    /***
     * Constructor for creating the BackOfCarCustomShape.
     */
    public BackOfCarCustomShape(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        defineShape(left,middle,right);
    }

    /***
     * Draws the ShapeDrawable onto the canvas.
     * @param Canvas canvas - The canvas to draw the ShapeDrawable onto
     */
    protected void onDraw(Canvas canvas) {
    	mDrawable.draw(canvas);
    }
	
    /***
     * This method defines the back of car shape providing the
     * shape with the current value of the left, middle and right sensors.
     * These 3 values are used to define certain aspects of the BackOfCarShape.
     * 
     * @param left - the value of the left sensor
     * @param middle - the value of the middle sensor
     * @param right - the value of the right sensor
     */
    public void defineShape(int left, int middle, int right){
	    int x = 10;
	    int y = 10;
	    int width = 300;
	    int height = 50;
	
	    mDrawable = new ShapeDrawable(new BackOfCarShape(left,middle,right));
	    mDrawable.getPaint().setColor(Color.BLACK);
	    mDrawable.setBounds(x, y, x + width, y + height);
    }
}
