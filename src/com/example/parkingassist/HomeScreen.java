package com.example.parkingassist;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/***
 * This class defines the Home screen activity.
 * This activity contains 4 buttons. Each button takes you to a
 * new activity.
 * 
 * Button 1 - StreamVideoView
 * Button 2 - BackOfCarView
 * Button 3 - TopOfCaeView
 * Button 4 - Disconnect
 * 
 * @author colinmcnicol
 *
 */
public class HomeScreen extends Activity {

	/***
	 * Sets up and creates the Activity using the appropriate layout
	 * xml file.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen);
		
	}
	
	/*** 
	 * This method is called when the back button of the phone is pressed.
	 * disconnect() is called
	 */
   @Override
    public void onBackPressed() {
    	disconnect();
    }
    
   	/***
   	 * This method is called when the back button on the UI is pressed.
   	 * disconnect() is called
   	 */
	public void disconnect(View v){
		disconnect();
	}
		
	/***
	 * This method sends a command to the pi telling it to close the
	 * connection made between the two devices.
	 * The home screen activity is closed and the user is taken back to the
	 * initial screen view.
	 */
	public void disconnect(){
		Bluetooth.send("ds");
        HomeScreen.this.finish();
	}
	
	/***
	 * This method is called when the Stream Video Button on the UI is pressed
	 * A new activity of the StreamVideoView is started and the current activity
	 * is closed.
	 * 
	 * A command is sent to the pi telling it what state is should go in
	 * therefore determining what data should be sent to the app.
	 * @param v - This is the current view
	 */
	public void streamVideoButton(View v){
		Bluetooth.send("sv");
        //start a new activity when stream video button is pressed
        Intent myIntent = new Intent(HomeScreen.this, StreamVideoView.class);
        HomeScreen.this.finish();
        HomeScreen.this.startActivity(myIntent);
	}
	
	/***
	 * This method is called when the Back Of Car View Button on the UI is pressed
	 * A new activity of the BackOfCarView is started and the current activity
	 * is closed.
	 * 
	 * A command is sent to the pi telling it what state is should go in
	 * therefore determining what data should be sent to the app.
	 * @param v - This is the current view
	 */
	public void backOfCarButton(View v){
		Bluetooth.send("bc");
        //start a new activity when back of car button is pressed
        Intent myIntent = new Intent(HomeScreen.this, BackOfCarView.class);
        HomeScreen.this.finish();
        HomeScreen.this.startActivity(myIntent);
	}
	
	/***
	 * This method is called when the Top Of Car View Button on the UI is pressed
	 * A new activity of the TopOfCarView is started and the current activity
	 * is closed.
	 * 
	 * A command is sent to the pi telling it what state is should go in
	 * therefore determining what data should be sent to the app.
	 * @param v - This is the current view
	 */
	public void topOfCarButton(View v){
		Bluetooth.send("tc");
        //start a new activity when top of car button is pressed
        Intent myIntent = new Intent(HomeScreen.this, TopOfCarView.class);
        HomeScreen.this.finish();
        HomeScreen.this.startActivity(myIntent);
	}
	
}
