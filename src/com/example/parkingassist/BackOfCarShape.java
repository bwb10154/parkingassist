package com.example.parkingassist;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.shapes.Shape;

/***
 * This class creates a new shape which can be drawn
 * onto the UI of the app.
 * This shape is designed for the BackOfCarView page on the UI.
 * 
 * @author colinmcnicol
 *
 */
public class BackOfCarShape extends Shape {

	static int leftSensor;
	static int middleSensor;
	static int rightSensor;
	static float left;
	static float middle;
	static float right;

	/***
	 * Constructor for creating the shape.
	 * The 3 parameters define the shape. They define how many arcs
	 * appear representing the distance of an object from each sensor.
	 * 
	 * @param left - current value of the left sensor
	 * @param middle - current value of the middle sensor
	 * @param right - current value of the right sensor
	 */
	public BackOfCarShape(int left, int middle, int right){
		leftSensor = left;
		middleSensor = middle;
		rightSensor = right;
	}
	
	/***
	 * Overrides the Shape class draw method.
	 * This defines the unique aspects of this shape.
	 * 3 rows of arcs are defined, representing the distance an object
	 * is from each of the 3 sensors. The smallest reading from
	 * the 3 sensors is displayed as well.
	 */
	@Override
	public void draw(Canvas c, Paint p) {
		
		// find the smallest of the 3 sensor readings
	    float smallest = left;
	    if (smallest > middle) smallest = middle;
	    if (smallest > right) smallest = right;
	    
	    
	    // Change colour of text and position depending on the value of the
	    // smallest sensor reading.
	    if(smallest >= 100){
	    	p.setTextSize(200);
	    	p.setColor(Color.GREEN);
	    	c.drawText("" + String.format("%.2f", smallest/100) + "m", 50, 300, p);
	    }
	    else if(smallest >= 30 && smallest < 100){
	    	p.setTextSize(200);
	    	p.setColor(Color.YELLOW);
	    	c.drawText("" + smallest + "cm", 20, 300, p);
	    }
	    else if(smallest >= 10 && smallest < 30){
	    	p.setTextSize(200);
	    	p.setColor(Color.RED);
	    	c.drawText("" + smallest + "cm", 20, 300, p);
	    }
	    else if(smallest < 10){
	    	p.setTextSize(200);
	    	p.setColor(Color.RED);
	    	c.drawText("" + smallest + "cm" , 80, 300, p);
	    }
	    
	    // Based on the value of the sensor, draw the appropriate number
	    // of arcs gradually increases the arc width ad changing the colour
	    // of the arc.
	    p.setStyle(Paint.Style.STROKE);
	    p.setStrokeWidth(10);
	    for( int i = 0; i < 180; i+=20){
	    	if( right > 300){break;}
		    if( i >= 20 && right <= 30){break;}
		    if( i >= 40 && right > 30 && right <= 60){break;}
		    if( i >= 60 && right > 60 && right <= 100){break;}
		    if( i >= 80 && right > 100 && right <= 130){break;}
		    if( i >= 100 && right > 130 && right <= 160){break;}
		    if( i >= 120 && right > 160 && right <= 200){break;}
		    if( i >= 140 && right > 200 && right <= 230){break;}
		    if( i >= 160 && right > 230 && right <= 260){break;}
		    
    		if( i <= 60 ){	p.setColor(Color.RED);	}
    		else if ( i > 60 && i <= 120 ){	p.setColor(Color.YELLOW);	}
    		else if( i > 120 ){	p.setColor(Color.GREEN);	}
    		
    		RectF rectF1 = new RectF(155-i, 610+i, 205+i, 630+i);
		    c.drawArc (rectF1, 0, 180, false, p);	
	    }
	    
	    // Based on the value of the sensor, draw the appropriate number
	    // of arcs gradually increases the arc width ad changing the colour
	    // of the arc.
	    for( int i = 0; i < 180; i+=20){
	    	if( middle > 300){break;}
		    if( i >= 20 && middle <= 30){break;}
		    if( i >= 40 && middle > 30 && middle <= 60){break;}
		    if( i >= 60 && middle > 60 && middle <= 100){break;}
		    if( i >= 80 && middle > 100 && middle <= 130){break;}
		    if( i >= 100 && middle > 130 && middle <= 160){break;}
		    if( i >= 120 && middle > 160 && middle <= 200){break;}
		    if( i >= 140 && middle > 200 && middle <= 230){break;}
		    if( i >= 160 && middle > 230 && middle <= 260){break;}
		    
    		if( i <= 60 ){	p.setColor(Color.RED);	}
    		else if ( i > 60 && i <= 120 ){	p.setColor(Color.YELLOW);	}
    		else if( i > 120 ){	p.setColor(Color.GREEN);	}
    		
    	    RectF rectF2 = new RectF(315-i, 610+i, 365+i, 630+i);
    	    c.drawArc (rectF2, 0, 180, false, p);	
	    }
	    
	    // Based on the value of the sensor, draw the appropriate number
	    // of arcs gradually increases the arc width ad changing the colour
	    // of the arc.
	    for( int i = 0; i < 180; i+=20){
	    	if( left > 300){break;}
		    if( i >= 20 && left <= 30){break;}
		    if( i >= 40 && left > 30 && left <= 60){break;}
		    if( i >= 60 && left > 60 && left <= 100){break;}
		    if( i >= 80 && left > 100 && left <= 130){break;}
		    if( i >= 100 && left > 130 && left <= 160){break;}
		    if( i >= 120 && left > 160 && left <= 200){break;}
		    if( i >= 140 && left > 200 && left <= 230){break;}
		    if( i >= 160 && left > 230 && left <= 260){break;}
		    
    		if( i <= 60 ){	p.setColor(Color.RED);	}
    		else if ( i > 60 && i <= 120 ){	p.setColor(Color.YELLOW);	}
    		else if( i > 120 ){	p.setColor(Color.GREEN);	}
    		    	    
    	    RectF rectF3 = new RectF(475-i, 610+i, 525+i, 630+i);
    	    c.drawArc (rectF3, 0, 180, false, p);	
	    }
	    
    
	}

}
