package com.example.parkingassist;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.shapes.Shape;

/***
 * This class creates a new shape which can be drawn
 * onto the UI of the app.
 * This shape is designed for the StreamVideoView page on the UI.
 * 
 * @author colinmcnicol
 *
 */
public class StreamViewShape extends Shape{

	/***
	 * Overrides the Shape class draw method.
	 * This defines the unique aspects of this shape.
	 */
	@Override
	public void draw(Canvas c, Paint p) {

		// LEFT HAND SIDE
		p.setStrokeWidth(8);
		p.setColor(Color.GREEN);
		c.drawLine(140, 538, 250, 400, p);		
		c.drawLine(200, 460, 250, 460, p);
		
		p.setStrokeWidth(10);
		p.setColor(Color.YELLOW);
		c.drawLine(90, 600, 140, 538, p);		
		c.drawLine(138, 543, 188, 543, p);
		
		p.setStrokeWidth(12);
		p.setColor(Color.RED);
		c.drawLine(50, 650, 90, 600, p);
		c.drawLine(88, 604, 138, 604, p);

		// MIDDLE
		p.setStrokeWidth(2);
		p.setColor(Color.GREEN);
		for(int i = 0; i < 5; i++){
			c.drawLine(262 + (45*i), 460, 292 + (45*i), 460, p);
		}
		
		p.setStrokeWidth(4);
		p.setColor(Color.YELLOW);
		for(int i = 0; i < 8; i++){
			c.drawLine(200 + (45*i), 543, 230 + (45*i), 543, p);
		}
		
		p.setStrokeWidth(6);
		p.setColor(Color.RED);
		for(int i = 0; i < 10; i++){
			c.drawLine(150 + (45*i), 604, 180+ (45*i), 604, p);
		}
				
		// RIGHT HAND SIDE
		p.setStrokeWidth(8);
		p.setColor(Color.GREEN);
		c.drawLine(560, 538, 450, 400, p);	
		c.drawLine(500, 460, 450, 460, p);
		
		p.setStrokeWidth(10);
		p.setColor(Color.YELLOW);
		c.drawLine(610, 600, 560, 538, p);
		c.drawLine(562, 543, 512, 543, p);
		
		p.setStrokeWidth(12);
		p.setColor(Color.RED);
		c.drawLine(650, 650, 610, 600, p);
		c.drawLine(612, 604, 558, 604, p);	
	}

}
