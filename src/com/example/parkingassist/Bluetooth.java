package com.example.parkingassist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/***
 * This class deals with all the Bluetooth functionality i.e. sending data via bluetooth,
 * receiving data via bluetooth and all the details with creating a connection to a bluetooth
 * device.
 * @author colinmcnicol
 *
 */
public class Bluetooth extends Activity implements OnItemClickListener {
 
    ArrayAdapter<String> listAdapter;
    ListView listView;
    BluetoothAdapter btAdapter;
    Set<BluetoothDevice> devicesArray;
    ArrayList<String> pairedDevices;
    ArrayList<BluetoothDevice> devices;
    public static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    protected static final int SUCCESS_CONNECT = 0;
    protected static final int MESSAGE_READ = 1;
    IntentFilter filter;
    BroadcastReceiver receiver;
    String tag = "debugging";
    static ConnectedThread connectedThread;
    
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Log.i(tag, "in handler");
            super.handleMessage(msg);
            
            switch(msg.what){
            case SUCCESS_CONNECT:
                // creates new thread
                connectedThread = new ConnectedThread((BluetoothSocket)msg.obj);
                Toast.makeText(getApplicationContext(), "CONNECT", 0).show();
                Log.i(tag, "connected");
                //start a new activity when connected through bluetooth

                Intent myIntent = new Intent(Bluetooth.this, HomeScreen.class);
                Bluetooth.this.finish();
                Bluetooth.this.startActivity(myIntent);
                
                
                break;
            case MESSAGE_READ:
            	//display any messages received
                byte[] readBuf = (byte[])msg.obj;
                String string = new String(readBuf);
                Toast.makeText(getApplicationContext(), string+": message read HERE",
                		Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_assist);
        init();
        //checks if a bluetooth adapter is available
        if(btAdapter==null){
            Toast.makeText(getApplicationContext(), "No bluetooth detected", 0).show();
            finish();
        }
        else{
            if(!btAdapter.isEnabled()){
            	//bluetooth is disabled so turn on bluetooth
                turnOnBT();
            }
             
            getPairedDevices();//get the paired devices
            startDiscovery(); //look for devices
        }
 
 
    }
    /***
     * when the back button of the phone is pressed, close the current activity and
     * open the InitialScreenView activity.
     */
    @Override
    public void onBackPressed() {
    	Intent myIntent = new Intent(Bluetooth.this, InitialScreenView.class);
        Bluetooth.this.finish();
        Bluetooth.this.startActivity(myIntent);
    }

    
    /**
     * This method looks for available bluetooth devices
     */
    private void startDiscovery() {
        btAdapter.cancelDiscovery();
        btAdapter.startDiscovery();  
    }
    
    /**
     * This method calls a function that requests the user to turn on their bluetooth
     */
    private void turnOnBT() {
        Intent intent =new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intent, 1);
    }
    
    /**
     * This method gets all the paired devices and stores them into a list
     */
    private void getPairedDevices() {
        devicesArray = btAdapter.getBondedDevices(); //retrieve paired devices and add to a list
        if(devicesArray.size()>0){
            for(BluetoothDevice device:devicesArray){
            	//adds paired device to an list of paired devices
                pairedDevices.add(device.getName());
            }
        }
        else{
        	//display that there are no paired devices
        	Toast.makeText(getApplicationContext(), "no paired devices",
        			Toast.LENGTH_SHORT).show();
        }
    }
    
    /**
     * this method returns a bluetooth adapter
     * @return BluetoothAdapter
     */
    public BluetoothAdapter getBtAdapter(){
    	return btAdapter;
    }
    
    /**
     * This method populates the list of paired devices and displays them on the screen
     * 
     */
    private void init() {
        listView=(ListView)findViewById(R.id.ListView1);
        listView.setOnItemClickListener(this);
        listAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,0);
        listView.setAdapter(listAdapter);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        pairedDevices = new ArrayList<String>();
        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        devices = new ArrayList<BluetoothDevice>();
        
        
        receiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if(BluetoothDevice.ACTION_FOUND.equals(action)){
                	
                    BluetoothDevice device = intent.getParcelableExtra(
                    		BluetoothDevice.EXTRA_DEVICE);
                    devices.add(device);
                    String s = "";
                    for(int a = 0; a < pairedDevices.size(); a++){
                        if(device.getName().equals(pairedDevices.get(a))){
                            //append 
                            s = "(Paired)";
                            break;
                        }
                    }
             
                    listAdapter.add(device.getName()+" "+s+" "+"\n"+device.getAddress());
                    Toast.makeText(getApplicationContext(), "Found Device",
                    		Toast.LENGTH_SHORT).show();

                }
                 
                else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
                    Toast.makeText(getApplicationContext(), "action discovery started",
                    		Toast.LENGTH_SHORT).show();
                }
                else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                    Toast.makeText(getApplicationContext(), "action discovery finished",
                    		Toast.LENGTH_SHORT).show();
                }
                else if(BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)){
                    if(btAdapter.getState() == btAdapter.STATE_OFF){
                        turnOnBT();
                    }
                }
           
            }
        };
         
        registerReceiver(receiver, filter);
         filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        registerReceiver(receiver, filter);
         filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(receiver, filter);
         filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(receiver, filter);
    }
     
    static int i = 0;
    
    /***
     * This method is used to send a string via bluetooth to the connected device.
     * @param s - the string that you wish to send via bluetooth
     */
    public static void send(String s){
    	byte[]x = s.getBytes();
    	connectedThread.write(x);
    }
    
    /***
     * This method is used to read an image sent via bluetooth
     * @return Bitmap of the received image
     */
    public static Bitmap readImage(){
    	Bitmap bm = connectedThread.read();
    	return bm;
    }
    
    /***
     * This method is used to read in the current value of
     * the left sensor sent from the pi.
     * @return float - value of the left sensor sent from pi.
     */
    public static float readLeftSensor(){
    	float left = connectedThread.readLeftSensor();
    	return left;
    }
    
    /***
     * This method is used to read in the current value of
     * the middle sensor sent from the pi.
     * @return float - value of the middle sensor sent from pi.
     */
    public static float readMiddleSensor(){
    	float middle = connectedThread.readMiddleSensor();
    	return middle;
    }
    
    /***
     * This method is used to read in the current value of
     * the right sensor sent from the pi.
     * @return float - value of the right sensor sent from pi.
     */
    public static float readRightSensor(){
    	float right = connectedThread.readRightSensor();
    	return right;
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
 
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED){
            Toast.makeText(getApplicationContext(), "Bluetooth must be enabled to continue",
            		Toast.LENGTH_SHORT).show();
            finish();
        }
    }
    
    /**
     * This method is evoked when a bluetooth device is selected from 
     * the list of bluetooth devices that are displayed on the screen.
     * This device is then tried to be connected too
     */
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
            long arg3) {
         
        if(btAdapter.isDiscovering()){
            btAdapter.cancelDiscovery(); // stop searching for devices
        }
        //check that the bluetooth device contains Paired
        if(listAdapter.getItem(arg2).contains("Paired")){
            BluetoothDevice selectedDevice = devices.get(arg2);
            //try to make a connection to the bluetooth device
            ConnectThread connect = new ConnectThread(selectedDevice);
            connect.start();
            Log.i(tag, "CLICKED ON LIST ITEM");
        }
        else{
            Toast.makeText(getApplicationContext(), "device is not paired", 0).show();
        }
    }
        /**
         * A class that is used to create a bluetooth connection using threads.
         */
        private class ConnectThread extends Thread {
         
            private final BluetoothSocket mmSocket;
            private final BluetoothDevice mmDevice;
          
            public ConnectThread(BluetoothDevice device) {
                // Use a temporary object that is later assigned to mmSocket,
                // because mmSocket is final
                BluetoothSocket tmp = null;
                mmDevice = device;
                Log.i(tag, "construct");
                // Get a BluetoothSocket to connect with the given BluetoothDevice
                try {
                    // MY_UUID is the app's UUID string, also used by the server code
                    tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                } catch (IOException e) { 
                    Log.i(tag, "get socket failed");
                     
                }
                mmSocket = tmp;
            }
          
            public void run() {
                // Cancel discovery because it will slow down the connection
                btAdapter.cancelDiscovery();
                Log.i(tag, "connect - run");
                try {
                    // Connect the device through the socket. This will block
                    // until it succeeds or throws an exception
                    mmSocket.connect();
                    Log.i(tag, "connect - succeeded");
                } catch (IOException connectException) {    
                	Log.i(tag, "connect failed");
                    // Unable to connect; close the socket and get out
                    try {
                        mmSocket.close();
                    } catch (IOException closeException) { }
                    return;
                }
          
                // Do work to manage the connection (in a separate thread)
            
                mHandler.obtainMessage(SUCCESS_CONNECT, mmSocket).sendToTarget();
            }
          
 
 
            // Will cancel an in-progress connection, and close the socket
            public void cancel() {
                try {
                    mmSocket.close();
                } catch (IOException e) { }
            }
        }
 
        /***
         * This class creates a thread which deals with sending and receiving
         * data via input and output streams to and from the connected 
         * bluetooth device.
         * @author colinmcnicol
         *
         */
        private class ConnectedThread extends Thread {
            private final BluetoothSocket mmSocket;
            private final InputStream mmInStream;
            private final OutputStream mmOutStream;
            BufferedReader bf;
          
            public ConnectedThread(BluetoothSocket socket) {
                mmSocket = socket;
                InputStream tmpIn = null;
                OutputStream tmpOut = null;
          
                // Get the input and output streams, using temp objects because
                // member streams are final
                try {
                    tmpIn = socket.getInputStream();
                    tmpOut = socket.getOutputStream();
                } catch (IOException e) { }
          
                mmInStream = tmpIn;
                mmOutStream = tmpOut;
                bf = new BufferedReader(new InputStreamReader(mmInStream));
            }
          
            public void run() {
                byte[] buffer;  // buffer store for the stream
                int bytes; // bytes returned from read()
     
                // Keep listening to the InputStream until an exception occurs
                while (true) {
                    try {
                        // Read from the InputStream
                        buffer = new byte[1024];
                        bytes = mmInStream.read(buffer);
                        // Send the obtained bytes to the UI activity
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                        
                    } catch (IOException e) {
                        break;
                    }
                }
            }
          
            /* Call this from the main activity to send data to the remote device */
            public void write(byte[] bytes) {
                try {
                    mmOutStream.write(bytes);
                } catch (IOException e) { }
            }
            
            long prevTime = 0;
            long currTime = 0;
            long diffTime = 0;
            
            /***
             * This method is used to read in an image sent from the pi.
             * The received image is decoded as a Bitmap so it can be displayed
             * on the UI.
             * @return Bitmap of the received image
             */
            public Bitmap read(){   
            	Bitmap bm = null;
            	
	        	  try {	                
	              	byte[] size = new byte[5];
	              	// read in the size of the image. Convert the bytes to a number.
	              	mmInStream.read(size);	
	              	
	              	String is = new String(size);
	              	// regex to remove any additional bits of data usually after the number which appear
	              	// if the number doesn't use up the 5 bytes which is the size of the size array.
	              	is = is.replaceAll("\\D+",""); 
	              	
	              	// Convert the string to an integer
	              	int imageSize = Integer.parseInt(is);
	              	Log.d("INFO", "Image size: " + imageSize);
	              	   	
	                // create new byte to be the size of the file
	                byte[] image = new byte[imageSize];	                
	                int bytes = 0;
	               
	                // read in the appropriate amount of bits using the image size received
	                // previously.
	                while(bytes < imageSize){
	                	bytes += mmInStream.read(image,bytes,image.length-bytes);
	                }
	                
	                Log.d("INFO", "bytes: " + bytes);	                
	                Log.d("INFO", "Create bit map");
	                
	                // use array and create a bitmap using the data in the array.
	                bm = BitmapFactory.decodeByteArray(image, 0, bytes);

	                currTime = System.nanoTime();
	                diffTime = currTime - prevTime;
	                Log.d("INFO", "time diff stamp: " + diffTime);
	                
	                prevTime = currTime;        	  
	        	  } catch (IOException e) { }
            	 
	        	 return bm;
            }
            
            /***
             * This method reads in the value of the left sensor sent from the pi.
             * @return float representing the value of the left sensor
             */
            public float readLeftSensor(){
            	String data = "";
              	try {
              		// read in a line of data. This will be the value of the left sensor
              		data = bf.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}	
              	// convert the string to a float
              	float leftSensor = Float.parseFloat(data);
              	Log.d("INFO", "Bluetooth - readLeftSensor - " + leftSensor);
              	return leftSensor;
            }
            
            /***
             * This method reads in the value of the middle sensor sent from the pi.
             * @return float representing the value of the middle sensor
             */
            public float readMiddleSensor(){
	        	String data = "";
	          	try {
	          	// read in a line of data. This will be the value of the middle sensor
	          		data = bf.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
	          	// convert the string to a float
	          	float middleSensor = Float.parseFloat(data);
	          	Log.d("INFO", "Bluetooth - readMiddleSensor - " + middleSensor);
	          	return middleSensor;
            }
          
            /***
             * This method reads in the value of the right sensor sent from the pi.
             * @return float representing the value of the right sensor
             */
            public float readRightSensor(){
	        	String data = "";
	          	try {
	          	// read in a line of data. This will be the value of the right sensor
	          		data = bf.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}	  
	          	// convert the string to a float
	          	float rightSensor = Float.parseFloat(data);
	          	Log.d("INFO", "Bluetooth - readRightSensor - " + rightSensor);
	          	return rightSensor;
            }
            
            /* Call this from the main activity to shutdown the connection */
            public void cancel() {
                try {
                    mmSocket.close();
                } catch (IOException e) { }
            }
        }
}
