package com.example.parkingassist;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/***
 * This class creates a new Activity.
 * This activity will display the back of the car view.
 * This view will consist of a sketch of the back of a car and
 * 3 rows of arcs will be drawn representing the distance each
 * sensor on the bumper is from an object.
 * 
 * This class reads in data sent from the pi. This data is used
 * to update the UI.
 * 
 * A button will also be provided allowing the user to turn
 * on and off the beeping sound warning the user when they
 * are getting closer to an object.
 * 
 * @author colinmcnicol
 *
 */
public class BackOfCarView extends Activity{

	asyncTask a;
	playSoundAsyncTask p;
	static boolean finishAsyncTask;
	static MediaPlayer slow;
	static MediaPlayer medium;
	static MediaPlayer fast;
	static boolean soundOnOff = false;
	

	/***
	 * Sets up and creates the Activity using the appropriate layout
	 * xml file.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Use the layout xml named back_of_car_view_xml to provide
		// the layout for this activity.
		setContentView(R.layout.back_of_car_view_xml);
		
		// Set up the 3 sound files used to provide a beeping sound
		// at different frequencies.
		slow = MediaPlayer.create(this, R.raw.bleepingslowspeed);
		
		medium = MediaPlayer.create(this, R.raw.bleepinghalfspeed);
		
		fast = MediaPlayer.create(this, R.raw.bleepingfullspeed);
		
		// start with the sound off
		soundOnOff = false;

		// Start the async task which is used to read in the sensor
		// data sent from the pi and update the UI accordingly.
		a = (asyncTask) new asyncTask().execute();

		// finishAsyncTask - used to exit from the async task.
		// Initially set to false.
		finishAsyncTask = false;
	}
	

	/***
	 * This method is called when the back button of the phone
	 * is pressed. The backButton() method is called from this method.
	 */
   @Override
    public void onBackPressed() {
    	backButton();
    }
    
   /***
    * This method is called when the back button on the UI 
    * is pressed. The backButton() method is called from this method.
    * 
    * @param v - This is the current View.
    */
	public void backButton(View v){
		backButton();
	}
	
	/***
	 * Called by either backButton(View v) or onBackPressed().
	 * This method sends command to the pi so that it stops sending
	 * data and exits its current state.
	 * This method closes down the current activity and opens the
	 * Home screen activity again.
	 */
	public void backButton(){
		
		// set finishAsyncTask to true. This will make the async task exit.
		finishAsyncTask = true;
		
		// send command to pi telling it to exit its current state and wait 
		// for the next state to be sent.
		Bluetooth.send("ns");
		
		// create new intent to open the home screen
        Intent myIntent = new Intent(BackOfCarView.this, HomeScreen.class);
        
        // BackOfCarView.this.startActivity(myIntent) - opens home screen
        BackOfCarView.this.startActivity(myIntent);
        
        // close this screen
        BackOfCarView.this.finish();
        
	}
	
	/***
	 * Method is called when the sound on/off button is pressed
	 * from the UI.
	 * This method keeps track of whether the sound is on or
	 * off and changes the variable to the other.
	 * 
	 * i.e. if the sound is currently on and the button is pressed,
	 * set the variable to false as the sound is now off.
	 * 
	 * If the sound is set to true, start the playSoundAsyncTask which
	 * deals with playing the appropriate sound at the correct time.
	 * 
	 * @param v - This is the current View.
	 */
	public void soundOnOffButton(View v){
		// if soundOnOff is currently true, change it to false
		if(soundOnOff == true){
			soundOnOff = false;
		}
		// else if soundOnOff is currently false, change it to true
		// and start the playSoundAsyncTask.
		else if(soundOnOff == false){
			soundOnOff = true;
			p = (playSoundAsyncTask) new playSoundAsyncTask().execute();
		}
	}
	
	/***
	 * Private Class.
	 * Used to asynchronously read in data sent from the pi and update the UI
	 * @author colinmcnicol
	 *
	 */
	private class asyncTask extends AsyncTask<Object, Object, Bitmap>{
		float leftSensor;
		float middleSensor;
		float rightSensor;
		int left = 0;
		int middle = 0;
		int right = 0;
		Bitmap bm;
			
			/***
			 * This method reads in data sent from the pi and sends an acknowledgment
			 * after it receives the data telling the pi to send more data.
			 * It also keeps checking to see if it should exit this method or not.
			 */
			@Override
			protected Bitmap doInBackground(Object... params) {
	        	
				while(true){
					Log.d("INFO", "BackOfCarView - do in background");					
					// left and right are round the wrong way as this is the view from the back
					// of the car therefore the sensors are flipped.
					// Read in data for the left, middle and right sensors
					leftSensor = Bluetooth.readRightSensor();
					middleSensor = Bluetooth.readMiddleSensor();
					rightSensor = Bluetooth.readLeftSensor();
					
					// publishProgress() is called which allows the UI to be updated.
		        	publishProgress(leftSensor,middleSensor,rightSensor);
		        	
		        	// If the finishAsyncTask variable has been set to true
		        	// i.e. the user wants to go back to the home screen,
		        	// break from this while loop.
		        	if(BackOfCarView.finishAsyncTask == true){
		        		break;
		        	}

		        	// send a command to the pi asking for more data to be sent
		        	Bluetooth.send("ok");
		        	
				}
				
				return bm;
			}	
			
			/***
			 * This method is used when publishProgress() is called. This allows
			 * the UI to be updated.
			 */
			@Override
			protected void onProgressUpdate(Object... values) {
				
				Log.d("INFO", "BackOfCarView - calc left value");
				
				// based on the readings from the sensors, calculate the value
				// for the variables left, middle and right. These are used to
				// update the graphics drawn on the UI.
				
				// set to 0 if the reading is between 1m and 3m
				if(leftSensor >= 100 && leftSensor < 300){
					left = 0;
				}
				// set to 1 if the reading is between 30cm and 1m
				else if(leftSensor > 30 && leftSensor < 100){
					left = 1;
				}
				// set to 2 if the reading is less than 30cm
				else if(leftSensor <= 30){
					left = 2;
				}
				// set to 3 if the reading is anything else
				// i.e. greater than 3m.
				else {
					left = 3;
				}
				
				// set to 0 if the reading is between 1m and 3m
				if(middleSensor >= 100 && middleSensor < 300){
					middle = 0;
				}
				// set to 1 if the reading is between 30cm and 1m
				else if(middleSensor > 30 && middleSensor < 100){
					middle = 1;
				}
				// set to 2 if the reading is less than 30cm
				else if(middleSensor <= 30){
					middle = 2;
				}
				// set to 3 if the reading is anything else
				// i.e. greater than 3m.
				else {
					middle = 3;
				}
				
				// set to 0 if the reading is between 1m and 3m
				if(rightSensor >= 100 && rightSensor < 300){
					right = 0;
				}
				// set to 1 if the reading is between 30cm and 1m
				else if(rightSensor > 30 && rightSensor < 100){
					right = 1;
				}
				// set to 2 if the reading is less than 30cm
				else if(rightSensor <= 30){
					right = 2;
				}
				// set to 3 if the reading is anything else
				// i.e. greater than 3m.
				else {
					right = 3;
				}
				
				Log.d("INFO", "BackOfCarView - LEFT = " + left);
				Log.d("INFO", "BackOfCarView - MIDDLE = " + middle);
				Log.d("INFO", "BackOfCarView - RIGHT = " + right);
	
				// update the static variables in BackOfCarShape.
				// These variables are used to determine certain aspects of the 
				// graphics which are drawn on the UI.
				BackOfCarShape.leftSensor = left;
				BackOfCarShape.middleSensor = middle;
				BackOfCarShape.rightSensor = right;
				
				BackOfCarShape.right = rightSensor;
				BackOfCarShape.middle = middleSensor;
				BackOfCarShape.left = leftSensor;
				
				// locate the custom shape which is to be updated based on the new results
				// received from the pi.
				BackOfCarCustomShape c = (BackOfCarCustomShape) findViewById(R.id.canvas2);				
				
				// the .invalidate() calls the draw method of BackOfCarCustomShape therefore
				// updating the UI with the new graphics.
				c.invalidate();	
								
			}

			/***
			 * Called when exiting the async task.
			 * This method does nothing.
			 */
			@Override
			protected void onPostExecute(Bitmap bm) {               
				Log.d("INFO", "BackOfCarView - onPostExecute");
		    }
		
	}
	
	/***
	 * Private Class
	 * This class asynchronously deals with playing the beeping sounds
	 * based on the data received from the pi.
	 * @author colinmcnicol
	 *
	 */
	private class playSoundAsyncTask extends AsyncTask<Object, Object, Bitmap>{

		Bitmap bm;
			
			/***
			 * This method runs in the background.
			 */
			@Override
			protected Bitmap doInBackground(Object... params) {
	        	
				while(true){
					// while true, keep calling publishProgress() updating the UI.
		        	publishProgress();
		        	
		        	// BackOfCarView.soundOnOff is false i.e. the sound is switched off,
		        	// break out of the while loop.
		        	if(BackOfCarView.soundOnOff == false){
		        		break;
		        	}		        			        	
				}
				
				return bm;
			}	
			
			/***
			 * This method checks all the appropriate values and determines what, if any, sounds
			 * should be played.
			 */
			@Override
			protected void onProgressUpdate(Object... values) {
				// if sound is on
				if( BackOfCarView.soundOnOff == true ){
					// update the button with the appropriate image showing that the sound is
					// currently on.
					Button b = (Button) findViewById(R.id.bSound1);
					b.setBackgroundResource(R.drawable.soundon);
					
					// check the values of the 3 sensors and if any are 2, this means the car is very close
					// to an object and so check for any sounds currently playing and if so stop them and start
					// the fast beeping sound.
					if(BackOfCarShape.leftSensor == 2 || BackOfCarShape.middleSensor == 2 
							|| BackOfCarShape.rightSensor == 2){					
						// stop the slow sound if it is currently playing
						if(BackOfCarView.slow.isPlaying()){
							BackOfCarView.slow.stop();
							try {
								// need to call prepare() so it prepares the sound so it can
								// be played again otherwise  it will cause problems when
								// .start() is called again.
								BackOfCarView.slow.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}	
						// stop the medium sound if it is currently playing
						if(BackOfCarView.medium.isPlaying()){
							BackOfCarView.medium.stop();
							try {
								// need to call prepare() so it prepares the sound so it can
								// be played again otherwise  it will cause problems when
								// .start() is called again.
								BackOfCarView.medium.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						// the fast sound needs to be played so check if its currently playing and if
						// so then great otherwise start the fast sound and set it to continuously loop.
						if(!BackOfCarView.fast.isPlaying()){
							BackOfCarView.fast.start();
							BackOfCarView.fast.setLooping(true);
						}					
					}
					
					// if neither of the values are 2 but at least one of then is 1, play the medium sound.
					else if(BackOfCarShape.leftSensor == 1 || BackOfCarShape.middleSensor == 1 
							|| BackOfCarShape.rightSensor == 1){
						if(BackOfCarView.slow.isPlaying()){
							BackOfCarView.slow.stop();
							try {
								BackOfCarView.slow.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						if(BackOfCarView.fast.isPlaying()){
							BackOfCarView.fast.stop();
							try {
								BackOfCarView.fast.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						if(!BackOfCarView.medium.isPlaying()){
							BackOfCarView.medium.start();
							BackOfCarView.medium.setLooping(true);
						}
					}
					
					// if neither of the values are 2 or 1, but at least one of them is 0, play the slow sound.
					else if(BackOfCarShape.leftSensor == 0 || BackOfCarShape.middleSensor == 0 
							|| BackOfCarShape.rightSensor == 0){
						if(BackOfCarView.medium.isPlaying()){
							BackOfCarView.medium.stop();
							try {
								BackOfCarView.medium.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						if(BackOfCarView.fast.isPlaying()){
							BackOfCarView.fast.stop();
							try {
								BackOfCarView.fast.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						if(!BackOfCarView.slow.isPlaying()){
							BackOfCarView.slow.start();
							BackOfCarView.slow.setLooping(true);
						}
					}
					// If neither of the above cases is satisfied i.e. the sensors aren't near any objects,
					// stop all the sounds.
					else{
						if(BackOfCarView.slow.isPlaying()){
							BackOfCarView.slow.stop();
							try {
								BackOfCarView.slow.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						if(BackOfCarView.medium.isPlaying()){
							BackOfCarView.medium.stop();
							try {
								BackOfCarView.medium.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
						if(BackOfCarView.fast.isPlaying()){
							BackOfCarView.fast.stop();
							try {
								BackOfCarView.fast.prepare();
							} catch (IllegalStateException e) {e.printStackTrace();
							} catch (IOException e) {e.printStackTrace();}
						}
					}	
				}
				
				// If BackOfCarView.soundOnOff is false, change the button image to represent
				// the sound being off and stop playing any sound if it is currently playing.
				else{
					Button b = (Button) findViewById(R.id.bSound1);
					b.setBackgroundResource(R.drawable.soundoff);
					if(BackOfCarView.slow.isPlaying()){
						BackOfCarView.slow.stop();
						try {
							BackOfCarView.slow.prepare();
						} catch (IllegalStateException e) {e.printStackTrace();
						} catch (IOException e) {e.printStackTrace();}
					}
					if(BackOfCarView.medium.isPlaying()){
						BackOfCarView.medium.stop();
						try {
							BackOfCarView.medium.prepare();
						} catch (IllegalStateException e) {e.printStackTrace();
						} catch (IOException e) {e.printStackTrace();}
					}
					if(BackOfCarView.fast.isPlaying()){
						BackOfCarView.fast.stop();
						try {
							BackOfCarView.fast.prepare();
						} catch (IllegalStateException e) {e.printStackTrace();
						} catch (IOException e) {e.printStackTrace();}
					}
				}
								
			}

			// Just before exiting from this asynchronous task, stop any sound that is playing.
			@Override
			protected void onPostExecute(Bitmap bm) {               
				Log.d("INFO", "BackOfCarView - onPostExecute");
				
				if(BackOfCarView.slow.isPlaying()){
					BackOfCarView.slow.stop();
				}
				if(BackOfCarView.medium.isPlaying()){
					BackOfCarView.medium.stop();
				}
				if(BackOfCarView.fast.isPlaying()){
					BackOfCarView.fast.stop();
				}
		    }
		
	}

}
