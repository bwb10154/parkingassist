package com.example.parkingassist;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.View;

/***
 * This class creates the Initial Screen activity.
 * This is the activity observed as soon as the app is run.
 * This activity contains a button used to find all nearby 
 * bluetooth devices and a button which displays an information
 * dialog box which provides information about the app.
 * 
 * @author colinmcnicol
 *
 */
public class InitialScreenView extends Activity {
	
	/***
	 * Sets up and creates the Activity using the appropriate layout
	 * xml file.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_initial_screen_view);			
	}

	/***
	 * This method is called when the find bluetooth devices button on
	 * the UI is pressed. This closes the current activity and opens
	 * the Bluetooth activity.
	 * 
	 * @param v - This is the current view
	 */
	public void findBluetoothDevices(View v){
	   // create new Intent
       Intent myIntent = new Intent(InitialScreenView.this, Bluetooth.class);
       // launch new activity
       InitialScreenView.this.startActivity(myIntent);
	}
	
	/***
	 * This method is called when the info button on the UI is pressed.
	 * A dialog box is displayed providing information about the app.
	 * 
	 * @param v - This is the current view
	 */
	public void appInfo(View v){
	    // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        
        // set the title, make it bold
        builder.setTitle(Html.fromHtml("<b>" + getString(R.string.title_app_info) +"</b>"));
        
        // This is the information displayed on the dialog.
        builder.setMessage(""
        		+ "To start using the app, press the 'Find Bluetooth Devices' button at the top of the screen. "
        		+ "All available bluetooth devices will be displayed. Select the raspberry-pi bluetooth "
        		+ "device and a connection will be made between the app and the raspberry pi. \n\n"
        		
        		+ "You will be taken to a page which consists of 4 buttons: \n"
        		+ "1. Steam Video View \n"
        		+ "2. Back of Car View \n"
        		+ "3. Top of Car View \n"
        		+ "4. Disconnect \n\n"
        		
        		+ "There are 3 different views that are available to choose from: \n\n"

        		+ "Stream Video View:- \n\n"

        		+ "This sends a stream of images from the pi camera module "
        		+ "enabling you to see the view from the back of the car without having to turn "
        		+ "around. \n\n\n"
        		
        		
        		+ "Back of Car View:- \n\n"
        		
        		+ "This displays a sketch of a the back of a car and highlights "
        		+ "where the sensors are placed. The distance each sensor is from an object is portrayed "
        		+ "through the use of arcs which increase in size as the distance increases. The shortest "
        		+ "value from all 3 sensors is displayed at the top of the screen. A beeping sound can "
        		+ "also be turned on and off from this screen to warn you when you are becoming too close "
        		+ "to an object.\n\n\n"

        		
        		+ "Top of Car View:- \n\n"
        		
        		+ "This displays a sketch of the top of a car and highlight the distance, "
        		+ "each sensor is from an object. A beeping sound can also be turned on and off from this "
        		+ "screen to warn you when you are becoming too close to an object. \n\n\n"

        		
        		+ "Disconnect:- \n\n"
        		
        		+ "This disconnects the app from the raspberry pi. The 'Find Bluetooth Devices' "
        		+ "button can be used to establish the connection again.\n\n\n"
        		
        		
        		+ "Developer Info:- \n\n"
        		+ "Colin McNicol \n"
        		+ "4th Year MEng Computer and Electronic Systems \n"
        		+ "4th Year Individual Project");
        	
        	// the dialog has an ok button. Clicking the button just closes the dialog.
        	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {// Do Nothing
                	   }
               });
        	
        // Create the AlertDialog object and show it.
        builder.create();
		builder.show();
	}

}
