package com.example.parkingassist;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.view.View;

/***
 * This class creates and instance of the StreamViewCustomShape 
 * and draws it onto a canvas.
 * 
 * @author colinmcnicol
 *
 */
public class StreamViewCustomShape extends View{

	private ShapeDrawable mDrawable;
	
    /***
     * Constructor for creating the StreamViewCustomShape.
     */
	public StreamViewCustomShape(Context context) {
		super(context);
		defineShape();
	}
	
    /***
     * Constructor for creating the StreamViewCustomShape.
     */
	public StreamViewCustomShape(Context context, AttributeSet attrs) {
		super(context, attrs,0);
		defineShape();
	}
	
    /***
     * Constructor for creating the StreamViewCustomShape.
     */
	public StreamViewCustomShape(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    defineShape();
	}
	
    /***
     * Draws the ShapeDrawable onto the canvas.
     * @param Canvas canvas - The canvas to draw the ShapeDrawable onto
     */
    protected void onDraw(Canvas canvas) {
    	mDrawable.draw(canvas);
    }
	
    /***
     * This method defines the stream view shape providing the
     * shape with the current value of the left, middle and right sensors.
     * These 3 values are used to define certain aspects of the StreamViewShape.
     */
    public void defineShape(){
	    int x = 10;
	    int y = 10;
	    int width = 300;
	    int height = 50;
	
	    mDrawable = new ShapeDrawable(new StreamViewShape());
	    mDrawable.getPaint().setColor(Color.BLACK);
	    mDrawable.setBounds(x, y, x + width, y + height);
    }
}
