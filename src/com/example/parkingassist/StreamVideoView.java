package com.example.parkingassist;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/***
 * This class creates a new Activity.
 * The activity will display an image stream sent from the pi.
 * 
 * @author colinmcnicol
 *
 */
public class StreamVideoView extends Activity {

	asyncTaskStream a;
	static boolean finishAsyncTask;
	
	/***
	 * Sets up and creates the Activity using the appropriate layout
	 * xml file.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Use the layout xml named stream_video_view_xml to provide
		// the layout for this activity.
		setContentView(R.layout.stream_video_view_xml);

		// Start the async task which is used to read in the image stream
		// data sent from the pi and update the UI accordingly with the new image.
		a = (asyncTaskStream) new asyncTaskStream().execute();
		
		// finishAsyncTask - used to exit from the async task.
		// Initially set to false
		finishAsyncTask = false;
	}
	
	/***
	 * This method is called when the back button of the phone
	 * is pressed. The backButton() method is called from this method.
	 */ 	
    @Override
    public void onBackPressed() {
    	backButton();
    }
    
    /***
     * This method is called when the back button on the UI 
     * is pressed. The backButton() method is called from this method.
     * 
     * @param v - This is the current View.
     */
	public void backButton(View v){
		backButton();
	}
	
	/***
	 * Called by either backButton(View v) or onBackPressed().
	 * This method sends command to the pi so that it stops sending
	 * data and exits its current state.
	 * This method closes down the current activity and opens the
	 * Home screen activity again.
	 */
	public void backButton(){
		
		// set finishAsyncTask to true. This will make the async task exit.
		finishAsyncTask = true;
		
		// send command to pi telling it to exit its current state and wait 
		// for the next state to be sent.
		Bluetooth.send("ns");
		
		// create new intent to open the home screen
        Intent myIntent = new Intent(StreamVideoView.this, HomeScreen.class);
        
        // close this screen
        StreamVideoView.this.finish();
        
        // StreamVideoView.this.startActivity(myIntent) - opens home screen
        StreamVideoView.this.startActivity(myIntent);
        
	}
	
	/***
	 * Private Class.
	 * Used to asynchronously read in image stream data sent from the pi and update the UI
	 * @author colinmcnicol
	 *
	 */
	public class asyncTaskStream extends AsyncTask<Object, Object, Bitmap>{
		Bitmap bm;
		float leftSensor;
		float middleSensor;
		float rightSensor;
			
		    /***
			 * This method reads in data sent from the pi and sends an acknowledgment
			 * after it receives the data telling the pi to send more data.
			 * It also keeps checking to see if it should exit this method or not.
			 */
			@Override
			protected Bitmap doInBackground(Object... params) {
	        	
				// Until told otherwise, continuously read in an image
				// and call the method to update the UI.
				while(true){
					Log.d("INFO", "StreamVideoView - doInBackground");
					
					// read in an image and store it in a Bitmap.
		        	bm = Bluetooth.readImage();
		        	
		        	// This method is used to update the UI.
		        	publishProgress(bm);
		        	
		        	// Check to see if the user has changed to a different page
		        	// and if so, break from this loop.
		        	if(StreamVideoView.finishAsyncTask == true){
		        		break;
		        	}
		        	
		        	// Ask for more data to be sent from the pi.
		        	Bluetooth.send("ok");
				}
				return bm;

			}	
			
			/***
			 * This method is used when publishProgress() is called. This allows
			 * the UI to be updated.
			 */
			@Override
			protected void onProgressUpdate(Object... values) {
				
				Log.d("INFO", "StreamVideoView - onProgressUpdate");
				
				// locate the image view where the image is displayed.
				ImageView iv = (ImageView) findViewById(R.id.imageView1);
				
				// set the image to the new bitmap of the imag esent from the pi.
				iv.setImageBitmap(bm);
			}


			/***
			 * Called when exiting the async task.
			 * This method does nothing.
			 */
			@Override
			protected void onPostExecute(Bitmap bm) {               
				Log.d("INFO", "StreamVideoView - onPostExecute");
		    }
		
	}

}
